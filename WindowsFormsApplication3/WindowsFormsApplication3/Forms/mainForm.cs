﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication3
{

  
    public partial class mainForm : Form
    {
        SqlConnection conn; 
        public mainForm( )
        {
            InitializeComponent();         
        }
    
        public void mainForm_Load(object sender, EventArgs e)
        {
            string connString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Александр\Desktop\WindowsFormsApplication3\WindowsFormsApplication3\example_DB.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";

            conn = new SqlConnection(connString);
            try
            {
                conn.Open();
            }
            catch (SqlException se)
            {
                errorLabel.Text = "Ошибка подключения к БД";
            }
        }

        private void regButton_Click(object sender, EventArgs e)
        {
            Form registrationForm = new registrationForm();
            registrationForm.Show();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            
            SqlCommand command = conn.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "StoredProcedure1";

            command.Parameters.AddWithValue("@Login", loginTextbox.Text);
            command.Parameters.AddWithValue("@Password",passwTextbox.Text);

           SqlParameter retValue = command.Parameters.Add("@return", SqlDbType.Int);
           retValue.Direction = ParameterDirection.ReturnValue; 
           command.ExecuteNonQuery();
           int pr = (int)retValue.Value;
           switch (pr)
           {
                case -100:
                    MessageBox.Show("Неверные логин/пароль", "Внимание");
                    break;
                case 100:
                    MessageBox.Show("Авторизация прошла успешно", "Внимание");
                    break;
            }        
        }
    }
}
